default:
  image: python:3.8-slim

variables:
  LOCAL_VERSION_LABEL: $CI_COMMIT_BRANCH
  RUN_UNIT_TEST: "true"
  RUN_SONARQUBE: "true"
  PUBLISH_RELEASE_ON_PYPI: "false"
  SOURCE_DIR: "opentf"
  TEST_DIR: "tests/python"
#"youpi bananane"
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      variables:
        LOCAL_VERSION_LABEL: MR-$CI_MERGE_REQUEST_IID
    # prevent double pipeline execution for "merge_request_event" event and "push" event (merge)
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    # Launch on tag with format vX.Y.Z
    - if: $CI_COMMIT_TAG =~ /^v(\d+)\.(\d+)\.(\d+)$/
     # Launch on tag with format vX.Y.Z
    - if: $CI_COMMIT_TAG =~ /^v(\d+)\.(\d+)\.(\d+)-rc(\d+)($|-.+)/

stages:
- validate
- test
- qa
- snapshot
- release-candidate
- release
- post-deploy

format-check:
  stage: validate
  before_script:
    # black install
    - pip install black
  script:
    # code format checking with black
    - black . --check --diff --color
  # No execution on "tag" event
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

version-check:
  stage: validate
  image: bitnami/git
  script:
    - REVISION=$(cat ./VERSION  | tr -d '[:space:]')
    - 'TAG=$(curl -s --header "PRIVATE-TOKEN: ${CI_BOT_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags/v${REVISION}")'
    - |
      if [[ "${TAG}" != "{\"message\":\"404 Tag Not Found\"}" ]]
      then
        echo "It already exists a released version for ${REVISION} in the project. A version bump is needed" && exit 1
      fi

  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

test:
  stage: test
  before_script:
    # Install required component
    - pip install unittest-xml-reporting coverage -e .
  script:
    # Unit test execution and coverage analysis
    - coverage run --source=${SOURCE_DIR} -m xmlrunner discover -s ${TEST_DIR} && coverage xml
  artifacts:
    # Unit tests and coverage reports stocked as gitlab-ci artifact in order to be used by next jobs
    paths:
      - TEST-*.xml
      - coverage.xml
    # Unit tests and coverage reports provided to gitlab for usage in gitlab UI
    reports:
      junit: TEST-*.xml
      cobertura: coverage.xml
    expire_in: 1 week
  # No execution on "tag" event
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    # Execution if TUN_UNIT_TEST is 'true' and there is file matching the pattern 'tests/python/test*.py'
    - if: $RUN_UNIT_TEST == "true"
      exists:
        - tests/python/test*.py

sonarcloud-check:
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  stage: qa
  retry: 1 #temporary fix for a Sonarcloud bug behaviour
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - >-
      sonar-scanner
      -Dsonar.projectVersion=$(cat ./VERSION  | tr -d '[:space:]') 
      -Dsonar.qualitygate.wait=true
      -Dsonar.python.version=3.8
      -Dsonar.organization=$SONARCLOUD_ORGANIZATION
      -Dsonar.login=$SONARCLOUD_TOKEN
      -Dsonar.host.url=$SONARCLOUD_HOST_URL #Overload with Sonarcloud variables
  dependencies:
    - test
  # No execution on "tag" event
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$RUN_SONARQUBE == "true"'
      when: on_success

sonarqube:
  image: sonarsource/sonar-scanner-cli
  stage: qa
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: $CI_JOB_NAME
    paths:
      - .sonar/cache
  before_script:
    # Update sonarKey and sonarName for main / dev separation in Sonarqube
    - sed -i "/sonar.projectKey/ s/$/$APPEND_SONAR_PROJECT_KEY/" sonar-project.properties
    - sed -i "/sonar.projectName/ s/$/$APPEND_SONAR_PROJECT_NAME/" sonar-project.properties
  script:
    - sonar-scanner -Dsonar.projectVersion=$(cat ./VERSION  | tr -d '[:space:]') -Dsonar.qualitygate.wait=true
  dependencies:
    - test
  # No execution on "tag" event
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
      # If we are on the 'main' branch we push analyse results in a specific 'main' project in sonarqube
      variables:
        APPEND_SONAR_PROJECT_KEY: ":main"
        APPEND_SONAR_PROJECT_NAME: " (main)"
    - when: on_success
      # If we are not on the 'main' branch the analysis results are pushed on the 'dev' project in sonarqube
      variables:
        APPEND_PROJECT_KEY: ":dev"
        APPEND_SONAR_PROJECT_NAME: " (dev)"


snapshot:
  stage: snapshot
  variables:
    TWINE_USERNAME: $PYPI_INTERNAL_REPO_USERNAME
    TWINE_PASSWORD: $PYPI_INTERNAL_REPO_PASSWORD
  before_script:
   # Twine install
   - pip install twine
  script:
    # Packaging
    - REVISION=$(cat ./VERSION  | tr -d '[:space:]')
    - BUILD_ID=".dev${CI_PIPELINE_IID}+`echo ${LOCAL_VERSION_LABEL} | tr -cd '[:alnum:].' | cut -c 1-14 `.${CI_COMMIT_SHORT_SHA}"
    - echo "VERSION=${REVISION}${BUILD_ID}" >> wheel_version
    - echo "COMMIT_HASH=${CI_COMMIT_SHA}" >> wheel_version
    - python setup.py egg_info -b "${BUILD_ID}" bdist_wheel
    # Publish on internal repository
    - twine upload --repository-url ${PYPI_INTERNAL_TEMP_REPO_URL} dist/*
    - echo RC build with sources ${CI_COMMIT_SHORT_SHA} and push in repo pypi-temp-hosted on Nexus
    - 'echo The published wheel is: $(ls dist/)'
    - cat wheel_version
  artifacts:
    # Unit tests and coverage reports stocked as gitlab-ci artifact in order to be used by next jobs
    paths:
    - wheel_version
    # Unit tests and coverage reports provided to gitlab for usage in gitlab UI
    expire_in: 1 hour
  dependencies: []
  rules:
    # No execution on "tag" event
    - if: $CI_COMMIT_TAG
      when: never
    # Manual execution on not protected branch
    - if: $CI_COMMIT_REF_PROTECTED == "false"
      when: manual
      allow_failure: true
    # Execution on protected branch
    - if: $CI_COMMIT_REF_PROTECTED == "true"


release-candidate:
  stage: release-candidate
  # Do a release-candidate on a Tag
  variables:
    TWINE_USERNAME: $PYPI_INTERNAL_REPO_USERNAME
    TWINE_PASSWORD: $PYPI_INTERNAL_REPO_PASSWORD
    REPO_URL: --repository-url ${PYPI_INTERNAL_ACCEPTANCE_REPO_URL}
    REPO_NAME: pypi-acceptance on Nexus
  before_script:
    # We check if the version in the tag match the version provided in the 'VERSION' file
    - REVISION=$(cat ./VERSION  | tr -d '[:space:]')
    - TAG_REVISION=$(echo "${CI_COMMIT_TAG}" | awk '{n=split($0,a,"-"); print a[1]}')
    - |
      if [[ "${TAG_REVISION}" != "v${REVISION}" ]]
      then 
        echo "The revison in the tag (${TAG_REVISION}) doesn't match the one defined in the pom (v${POM_REVISION})" && exit 1
      fi
    # Define the rc version
    - RC_TEMP_VERSION=${REVISION}$(echo "${CI_COMMIT_TAG}" | awk '{n=split($0,a,"-"); print a[2]}')
    - echo "${RC_TEMP_VERSION}" > ./VERSION
    # Twine install
    - pip install twine
  script:
    # Packaging
    - echo ${REPO_NAME}
    - python setup.py egg_info bdist_wheel
    # Publish
    - twine upload ${REPO_URL} dist/*
    - 'echo Release-candidate ${RC_TEMP_VERSION} with sources ${CI_COMMIT_SHORT_SHA} and pushed in ${REPO_NAME}'
    - 'echo The publisehd wheel is: $(ls dist/)'
  dependencies: []
  rules:
    # Execution on protected 'Tag' matching the pattern /^v(\d+)\.(\d+)\.(\d+)-rc(\d+)($|-.+)/
    # rc are always published on our private repo
    - if: '$CI_COMMIT_TAG =~ /^v(\d+)\.(\d+)\.(\d+)-rc(\d+)($|-.+)/ &&  $CI_COMMIT_REF_PROTECTED == "true"'

release:
  stage: release
  # Release a version on Tag
  before_script:
    # We check if the version in the tag match the version provided in the 'VERSION' file
    - export REVISION=$(cat ./VERSION  | tr -d '[:space:]')
    - |
      if [[ "${CI_COMMIT_TAG}" != "v${REVISION}" ]]
      then
        echo "The revison in the tag (${CI_COMMIT_TAG}) doesn't match the one defined in the pom (v${POM_REVISION})" && exit 1
      fi
    # Twine install
    - pip install twine
  script:
    # Packaging
    - echo ${REPO_NAME}
    - python setup.py egg_info bdist_wheel
    # Publish
    - twine upload ${REPO_URL} dist/*
    - 'echo Release of ${CI_COMMIT_TAG} with sources ${CI_COMMIT_SHORT_SHA} and pushed in ${REPO_NAME}'
    - 'echo The publisehd wheel is: $(ls dist/)'
  dependencies: []
  rules:
    # Execution on protected 'Tag' matching the pattern /^v(\d+)\.(\d+)\.(\d+)$/
    # And if $PUBLISH_RELEASE_ON_PYPI is not "true" the publication is done in the internal nexus
    - if: '$CI_COMMIT_TAG =~ /^v(\d+)\.(\d+)\.(\d+)$/ &&  $CI_COMMIT_REF_PROTECTED == "true" && $PUBLISH_RELEASE_ON_PYPI != "true"'
      variables:
        TWINE_USERNAME: $PYPI_INTERNAL_REPO_USERNAME
        TWINE_PASSWORD: $PYPI_INTERNAL_REPO_PASSWORD
        REPO_URL: --repository-url ${PYPI_INTERNAL_RELEASE_REPO_URL}
        REPO_NAME: pypi-hosted on Nexus
    # Execution on protected 'Tag' matching the pattern /^v(\d+)\.(\d+)\.(\d+)$/
    # And if $PUBLISH_RELEASE_ON_PYPI is "true" the publication is done in official pypi
    - if: '$CI_COMMIT_TAG =~ /^v(\d+)\.(\d+)\.(\d+)$/ &&  $CI_COMMIT_REF_PROTECTED  == "true" && $PUBLISH_RELEASE_ON_PYPI == "true"'
      variables:
        TWINE_USERNAME: $PYPI_PROD_REPO_USERNAME
        TWINE_PASSWORD: $PYPI_PROD_REPO_PASSWORD
        REPO_URL: ""
        REPO_NAME: pypi official

upload-version-file:
  stage: post-deploy
  image: bitnami/git
  variables:
    LATEST_VERSION_FILE: ${CI_COMMIT_BRANCH}-latest
  dependencies:
    - snapshot
  script:
    - PROJECT_NAME=$(grep -m1 name= setup.py | grep -oP '(?<=name='\''|").*(?='\''|")')
    - mv wheel_version ${LATEST_VERSION_FILE}
    - cat ${LATEST_VERSION_FILE}
    - 'curl -sSL -i --user "${RAW_REPO_USERNAME}:${RAW_REPO_PASSWORD}" --upload-file ./${LATEST_VERSION_FILE} ${RAW_INTERNAL_PROJECT_REPO_URL}/otf/version-tracking/${PROJECT_NAME}/${LATEST_VERSION_FILE}'
  rules:
    - if: '$CI_COMMIT_BRANCH == "main" || $CI_COMMIT_BRANCH =~ /^rel-(.+)$/'


build-image-trigger:
  image: bitnami/git
  stage: post-deploy
  script: 
    - ORCHESTRATOR_VERSION=$(grep VERSION wheel_version | grep -oP '(?<=VERSION=).*')
    # We trigger by api call the pipeline which will build the OTF all-in-one docker image
    - >-
      curl --fail --request POST 
      --form token=${CI_JOB_TOKEN} --form ref=${OTF_IMAGE_REF_BRANCH} 
      --form "variables[ORCHESTRATOR_VERSION]=${ORCHESTRATOR_VERSION}" 
      "${CI_API_V4_URL}/projects/${OTF_IMAGE_PROJECT_ID}/trigger/pipeline"
  rules:
    # This job runs when a commit is done on the "main" branch or when launched through the gitlab UI
    - if: $CI_COMMIT_BRANCH == "main"
    - if: '$CI_PIPELINE_SOURCE == "web"  && $ORCHESTRATOR_VERSION != "" '
